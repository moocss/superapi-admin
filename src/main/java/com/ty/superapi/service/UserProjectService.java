package com.ty.superapi.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ty.superapi.dao.UserProjectDao;
import com.ty.superapi.entity.UserProject;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tianyu
 * @since 2017-12-21
 */
@Service
public class UserProjectService extends ServiceImpl<UserProjectDao, UserProject> {
	
}
