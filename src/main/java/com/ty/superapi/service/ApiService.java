package com.ty.superapi.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.ty.superapi.dao.ApiDao;
import com.ty.superapi.entity.Api;
import com.ty.superapi.entity.Project;
import com.xiaoleilu.hutool.date.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tianyu
 * @since 2017-12-21
 */
@Service
@Transactional(readOnly = true)
public class ApiService extends ServiceImpl<ApiDao, Api> {

    @Autowired
    private ProjectService projectService;

    /**
     * 新增或修改api
     *
     * @param api
     * @return
     */
    @Transactional(readOnly = false)
    public Boolean put(Api api) {
        EntityWrapper<Project> entityWrapper = new EntityWrapper<Project>();
        entityWrapper.eq("id", api.getProjectId());
        Project project = projectService.selectOne(entityWrapper);
        project.setUpdateDate(DateUtil.date());
        projectService.updateById(project);
        api.setUpdateDate(DateUtil.date());
        return insertOrUpdate(api);
    }
}
